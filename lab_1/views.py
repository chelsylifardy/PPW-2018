from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Chelsy Lifardy' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998,9,24) #format (Year, Month, Date)
npm = 1706043582 # TODO Implement this

mhs_name1 = 'Darell Hendry'
curr_year1 = int(datetime.now().strftime("%Y"))
birth_date1 = date(1999,9,2) #format (Year, Month, Date)
npm1 = 1706044023

mhs_name2 = 'Irfan'
curr_year2 = int(datetime.now().strftime("%Y"))
birth_date2 = date(1999,6,10) #format (Year, Month, Date)
npm2 = 1706039585

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,'name1': mhs_name1, 'age1': calculate_age(birth_date1.year), 'npm1': npm1,
                'name2': mhs_name2, 'age2': calculate_age(birth_date2.year), 'npm2': npm2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
